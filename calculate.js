const { isPrime } = require('./helpers');

const checkCircular = strNum => {
  const primeMap = {};
  let curNum = strNum;
  for (let i = 0; i < strNum.length; i++) {
    if (!primeMap.hasOwnProperty(curNum)) {
      primeMap[curNum] = isPrime(Number(curNum));
    }
    if (!primeMap[curNum]) {
      return false
    }
    curNum = curNum.slice(1) + curNum.slice(0, 1);
  }
  return true;
}

const digits = ['1', '3', '7', '9'];

const generatePermutations = capacity => {
  if (capacity === 1) {
    return [...digits];
  }
  const suffixes = generatePermutations(capacity - 1);
  return digits.reduce((acc, digit) => acc.concat(suffixes.map(suffix => digit + suffix)), []);
}

const findCircular = maxCapacity => {
  let res = ['2', '3', '5', '7'];
  for (let i = 2; i <= maxCapacity; i++) {
    res = res.concat(generatePermutations(i).filter(checkCircular));
  }
  return res;
}

exports.findCircular = findCircular;
