const { expect } = require('chai');
const { isPrime } = require('../helpers');

describe('Helpers', () => {
  describe('isPrime', () => {
    it('should be true if param is 1', () => {
      expect(isPrime(1)).to.be.false;
    });
    it('should be true if param is 2', () => {
      expect(isPrime(2)).to.be.true;
    });
    it('should be true if param is 3', () => {
      expect(isPrime(3)).to.be.true;
    });
    it('should be true if param is 4', () => {
      expect(isPrime(4)).to.be.false;
    });
    it('should be true if param is 5', () => {
      expect(isPrime(5)).to.be.true;
    });
    it('should be true if param is 6', () => {
      expect(isPrime(6)).to.be.false;
    });
    it('should be true if param is 7', () => {
      expect(isPrime(7)).to.be.true;
    });
    it('should be true if param is 8', () => {
      expect(isPrime(8)).to.be.false;
    });
    it('should be true if param is 9', () => {
      expect(isPrime(9)).to.be.false;
    });
    it('should be true if param is 10', () => {
      expect(isPrime(10)).to.be.false;
    });
    it('should be true if param is 11', () => {
      expect(isPrime(11)).to.be.true;
    });
    it('should be true if param is 49', () => {
      expect(isPrime(49)).to.be.false;
    });
    it('should be true if param is 119', () => {
      expect(isPrime(119)).to.be.false;
    });
  });
});
