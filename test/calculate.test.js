const { expect } = require('chai');
const { findCircular } = require('../calculate');
const circulars = require('./circular-primes.mock');

describe('Calculate', () => {
  describe('findCircular', () => {
    it('should return exact list of numbers from first 10 numbers', () => {
      const result = findCircular(1);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes10);
    });
    it('should return exact list of numbers from first 100 numbers', () => {
      const result = findCircular(2);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes100);
    });
    it('should return exact list of numbers from first 1000 numbers', () => {
      const result = findCircular(3);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes1000);
    });
    it('should return exact list of numbers from first 10000 numbers', () => {
      const result = findCircular(4);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes10000);
    });
    it('should return exact list of numbers from first 100000 numbers', () => {
      const result = findCircular(5);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes100000);
    });
    it('should return exact list of numbers from first 1000000 numbers', () => {
      const result = findCircular(6);
      expect(result)
        .to.be.an('array')
        .and.to.be.deep.equal(circulars.circularPrimes1000000);
    });
  });
});

