const circularPrimes = [ 
  '2',
  '3',
  '5',
  '7',
  '11',
  '13',
  '17',
  '31',
  '37',
  '71',
  '73',
  '79',
  '97',
  '113',
  '131',
  '197',
  '199',
  '311',
  '337',
  '373',
  '719',
  '733',
  '919',
  '971',
  '991',
  '1193',
  '1931',
  '3119',
  '3779',
  '7793',
  '7937',
  '9311',
  '9377',
  '11939',
  '19391',
  '19937',
  '37199',
  '39119',
  '71993',
  '91193',
  '93719',
  '93911',
  '99371',
  '193939',
  '199933',
  '319993',
  '331999',
  '391939',
  '393919',
  '919393',
  '933199',
  '939193',
  '939391',
  '993319',
  '999331'
];
const circularPrimes10 = circularPrimes.filter(str => str.length < 2);
const circularPrimes100 = circularPrimes.filter(str => str.length < 3);
const circularPrimes1000 = circularPrimes.filter(str => str.length < 4);
const circularPrimes10000 = circularPrimes.filter(str => str.length < 5);
const circularPrimes100000 = circularPrimes.filter(str => str.length < 6);
const circularPrimes1000000 = circularPrimes.filter(str => str.length < 7);

exports.circularPrimes = circularPrimes;
exports.circularPrimes10 = circularPrimes10;
exports.circularPrimes100 = circularPrimes100;
exports.circularPrimes1000 = circularPrimes1000;
exports.circularPrimes10000 = circularPrimes10000;
exports.circularPrimes100000 = circularPrimes100000;
exports.circularPrimes1000000 = circularPrimes1000000;
