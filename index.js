const { findCircular } = require('./calculate');

const [,,arg] = process.argv;
const maxCapacity = Number(arg);
if (Number.isNaN(maxCapacity) || maxCapacity < 1) {
  console.error(`Wrong parameter or parameter is absend. Got ${arg}. Expect number 1 or above`);
  process.exit();
}

const numbers = findCircular(maxCapacity);

console.log('Find circular. Result', numbers);
